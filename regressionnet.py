import torch.nn as nn
class RegressionNet(nn.Module):
    def __init__(self):
        super(RegressionNet, self).__init__()
        self.conv1=nn.Conv2d(2,16,4,stride=2)
        self.relu1=nn.ReLU()
        self.conv2=nn.Conv2d(16,32,3,stride=2)
        self.relu2=nn.ReLU()
        self.conv3=nn.Conv2d(32,64,3,stride=2)
        self.relu3=nn.ReLU()
        self.conv4=nn.Conv2d(64,128,3,stride=2)
        self.relu4=nn.ReLU()

        self.fc1= nn.Linear(1152,256)
        self.fc2= nn.Linear(256,64)
        self.fc3= nn.Linear(64,1)

    def forward(self, im):
        x=self.conv1.forward(im)
        x=self.relu1.forward(x)
        x=self.conv2.forward(x)
        x=self.relu2.forward(x)
        x=self.conv3.forward(x)
        x=self.relu3.forward(x)
        x=self.conv4.forward(x)
        x=self.relu4.forward(x)
        x = x.view(x.size(0), -1)
        x=self.fc1.forward(x)
        x=self.fc2.forward(x)
        x=self.fc3.forward(x)
        return x.squeeze()
