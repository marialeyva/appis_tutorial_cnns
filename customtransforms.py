from scipy.ndimage import rotate
import numbers
import numpy as np


class MatrixRandomRotation(object):
    """Rotate the image by angle.

    Args:
        degrees (sequence or float or int): Range of degrees to select from.
            If degrees is a number instead of sequence like (min, max), the range of degrees
            will be (-degrees, +degrees).
    """

    def __init__(self, degrees):
        if isinstance(degrees, numbers.Number):
            if degrees < 0:
                raise ValueError("If degrees is a single number, it must be positive.")
            self.degrees = (-degrees, degrees)
        else:
            if len(degrees) != 2:
                raise ValueError("If degrees is a sequence, it must be of len 2.")
            self.degrees = degrees

    def __call__(self, img):
        """
            img (np array): Image to be rotated.

        Returns:
            np array: Rotated image.
        """

        angle = np.random.random_integers(self.degrees[0], self.degrees[1])

        return rotate(img, angle)



class MatrixCentralCrop(object):
    """Crop center of the image

    Args:
        size: size of the crop
    """

    def __init__(self, size):
        self.size=size

    def __call__(self, img):
        """
            img (np array): Image to be cropped.

        Returns:
            np array: cropped image.
        """

        y,x,z = img.shape
        startx = x//2-(self.size//2)
        starty = y//2-(self.size//2)    
        return img[starty:starty+self.size,startx:startx+self.size]
