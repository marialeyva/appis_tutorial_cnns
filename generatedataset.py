import h5py
from tqdm import tqdm as tqdm
import numpy as np
import pandas as pd
from tcirdataset import TCIRDataSet
from torch.utils.data import DataLoader
import torchvision.transforms as ttf

#This script processes the TCIR dataset as downloaded from https://www.csie.ntu.edu.tw/~htlin/program/TCIR/
#Divides it between train and test and calculates and saves mean and stardard deviation
dataset_dir="" #to be set to dataset location
def generate_dataset():
    dataset_file =dataset_dir+"TCIR-ATLN_EPAC_WPAC.h5"

    data_info = pd.read_hdf(dataset_file, key="info", mode='r')


    #Train with before 2015, and test with 2015 and after
    train_indices=np.where(np.array(data_info["time"]).astype(int)<2015000000)[0]
    test_indices=np.where(np.array(data_info["time"]).astype(int)>2015000000)[0]
    with h5py.File(dataset_dir+'train.h5', 'w') as h:
        with h5py.File(dataset_file, "r") as f:
            s=(f["matrix"].shape)
            dset = h.create_dataset('matrix', shape=(len(train_indices),s[1],s[2],s[3]), dtype=f["matrix"].dtype)
            labels = h.create_dataset('labels', shape=(len(train_indices),), dtype=f["matrix"].dtype)
            for i, idx in tqdm(enumerate(train_indices)):
                im =np.nan_to_num(f["matrix"][idx,:,:,:])
                im[im > 1000] = 0
                dset[i]=im
                labels[i]=data_info["Vmax"][idx]


    with h5py.File(dataset_dir+'test.h5', 'w') as h:
        with h5py.File(dataset_file, "r") as f:
            s=(f["matrix"].shape)
            dset = h.create_dataset('matrix', shape=(len(test_indices),s[1],s[2],s[3]), dtype=f["matrix"].dtype)
            labels = h.create_dataset('labels', shape=(len(test_indices),), dtype=f["matrix"].dtype)
            for i, idx in tqdm(enumerate(test_indices)):
                im =np.nan_to_num(f["matrix"][idx,:,:,:])
                im[im > 1000] = 0
                dset[i]=im
                labels[i]=data_info["Vmax"][idx]

                
def calculate_mean_std():
    dataset = TCIRDataSet(dataset_dir+'train.h5', transform=ttf.ToTensor())
    loader = DataLoader(
        dataset,
        batch_size=10,
        num_workers=1,
        shuffle=False
    )
    mean = 0.
    std = 0.
    nb_samples = 0.
    for data in tqdm(loader):
        batch_samples = data["im"].size(0)
        data = data["im"].view(batch_samples, data["im"].size(1), -1)
        mean += data.mean(2).sum(0)
        std += data.std(2).sum(0)
        nb_samples += batch_samples

    mean /= nb_samples
    std /= nb_samples
    print(mean, std)
    np.save(dataset_dir+"mean.npy",mean)
    np.save(dataset_dir+"std.npy",std)
    
    
if __name__ == "__main__":
    generate_dataset()
    calculate_mean_std()