from torch.utils.data import Dataset
import h5py
import numpy as np

class TCIRDataSet(Dataset):
    def __init__(self, datafile, transform=None, keepchannels=[0,3]):
        """
        Args:
            datafile (string): Path to the dataset file (.h5)
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.datafile=datafile
        self.transform = transform
        self.keepchannels=keepchannels


    def __len__(self):
        with h5py.File(self.datafile, "r") as f:
            return f["labels"].shape[0]

    def __getitem__(self, idx):
        with h5py.File(self.datafile, "r") as f:
            im=f["matrix"][idx,:,:,:]
            label = f["labels"][idx]
        if self.transform is not None:
            im = self.transform(im)
        return {"im":im[self.keepchannels,:,:], "label": label}
